import os
import sys
import subprocess
import time
import timeit
import datetime
import matplotlib.pyplot as plt

if len(sys.argv) == 1:
    pingCount = 3600
elif len(sys.argv) == 2:
    try:
        pingCount = int(sys.argv[1])
    except:
        print("Error: Print count must be a valid integer")
        exit()
else:
    print("Error: Invalid number of input arguments")
    exit()

hostname = os.getenv("computername")
homedrive = os.getenv("homedrive")
homepath = os.getenv("homepath")

packetSize = 32

sandboxPath = os.path.join("{0}{1}".format(homedrive,homepath),"Sandbox")

if not os.path.isdir(sandboxPath):
    os.mkdir(sandboxPath)

dataFilepath = os.path.join(sandboxPath,"network-output.txt")
lastTime = -1
currentPingCount = 0
if not os.path.isfile(dataFilepath):
    with open(dataFilepath,"w",newline="",encoding="utf-8") as f:
        while currentPingCount < pingCount:
            if timeit.default_timer()-lastTime > 1:
                lastTime = timeit.default_timer()
                ping = -1
                success = -1
                address = ""
                try:
                    response = subprocess.check_output("ping www.google.com -n 1 -w 1",shell=True).decode("utf-8")
                except:
                    response = "Request timed out."
                for line in response.replace("\r","").strip().split("\n"):
                    if "Request timed out" in line:
                        ping = "*"
                        success = 0
                        address = "*"
                    elif line.startswith("Reply from"):
                        address = line.split(":")[0].strip().lstrip("Reply from").strip()
                        results = line.split(":")[1].strip().split(" ")
                        for result in results:
                            if result.startswith("time"):
                                ping = int(result.split("=")[1].rstrip("ms"))
                                success = 1
                                break
                currentPingCount += 1
                if ping != -1:
                    f.write("{0} {1} {2} {3} {4} {5}\n".format(str(currentPingCount).rjust(6),hostname.ljust(20),address.ljust(20) if success == 1 else "*".ljust(20),str(ping).rjust(8),str(packetSize).rjust(8) if success == 1 else "*".rjust(8),"Success" if success == 1 else "TimedOut"))
                    print("[{0}] {1} {2} {3} {4} {5} {6}".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),str(currentPingCount).rjust(6),hostname.ljust(20),address.ljust(20) if success == 1 else "*".ljust(20),str(ping).rjust(8),str(packetSize).rjust(8) if success == 1 else "*".rjust(8),"Success" if success == 1 else "TimedOut"))
                else:
                    print("Error: Unexpected response from ping command on attempt {0}:\n\n{1}".format(currentPingCount,response))
                    exit()
            else:
                time.sleep(0.1)

with open(dataFilepath,"r",newline="",encoding="utf-8") as f:
    data = f.read()

lines = data.strip().replace("\r","").split("\n")

count = 0
x = []
ping = []
success = []
started = False

for line in lines:
    if not(started) and not(line.strip().startswith("1")):
        continue
    else:
        started = True
    if line.strip() == "":
        continue
    lineSplit = line.strip().split(" ")
    idx = len(lineSplit)
    for i in range(len(lineSplit)):
        idx -= 1
        if lineSplit[idx] == "":
            lineSplit.pop(idx)
    count += 1
    x += [int(lineSplit[0])]
    if lineSplit[-1] == "Success":
        success += [0]
        ping += [int(lineSplit[3])]
    else:
        success += [1]
        ping += [999]

plt.plot(x,ping)
plt.show()

plt.plot(x,success)
plt.show()
